//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Halem, Sam on 3/15/23.
//

import XCTest
@testable import NYCSchools

final class SchoolsViewModelTests: XCTestCase {

    var viewModel: SchoolsViewModel!
        
        override func setUp() {
            super.setUp()
            viewModel = SchoolsViewModel()
        }
        
        override func tearDown() {
            viewModel = nil
            super.tearDown()
        }
        
        func test_fetchAllSchools() {
            // Given
            let expectation = XCTestExpectation(description: "Fetching all schools")
            
            // When
            viewModel.fetchAllSchools()
            
            // Then
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                XCTAssertGreaterThan(self.viewModel.schools.count, 0, "Schools array is empty")
                expectation.fulfill()
            }
            
            wait(for: [expectation], timeout: 10.0)
        }

}

