//
//  SwiftUIView.swift
//  NYCSchoolsTests
//
//  Created by Halem, Sam on 3/16/23.
//

import SwiftUI
import Foundation

struct SingleLabelCardView: View {
    
    let label: String
    let size: CGFloat
    
    var body: some View {
        VStack {
            Text(label)
                .font(.system(size: size))
                .foregroundColor(.white)
                .padding()
        }
        .frame(maxWidth: .infinity)
        .background(Color.blue)
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.35), radius: 5, x: 0, y: 2)
    }
}
